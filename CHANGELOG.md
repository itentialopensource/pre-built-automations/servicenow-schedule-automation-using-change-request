
## 0.2.7 [01-03-2024]

* Remove img tag from markdown file

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!17

---

## 0.2.6 [09-23-2023]

* add deprecation information

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!16

---

## 0.2.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!15

---

## 0.2.4 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!14

---

## 0.2.3 [12-21-2021]

* certified for 2021.2

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!13

---

## 0.2.2 [07-09-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!12

---

## 0.2.1 [03-19-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!11

---

## 0.2.0 [06-18-2020]

* Add './' to img path

See merge request itentialopensource/pre-built-automations/servicenow-schedule-automation-using-change-request!5

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab

See merge request itentialopensource/pre-built-automations/ServiceNow-Schedule-Automation-Using-Change-Request!4

---

## 0.0.2 [05-29-2020]

* Update package.json, manifest.json, README.md, bundles/workflows/ServiceNow...

See merge request itentialopensource/pre-built-automations/staging/ServiceNow-Schedule-Automation-Using-Change-Request!3

---\n\n\n\n
