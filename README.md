<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 08-24-2023 and will be end of life on 06-30-2024. The capabilities of this Pre-Built have been replaced by the [Itential ServiceNow Application](https://store.servicenow.com/sn_appstore_store.do#!/store/application/f2ba728813da041032813092e144b01f) available in the ServiceNow Store.

<!-- Update the below line with your pre-built name -->
# ServiceNow Schedule Automation Using Change Request

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [ServiceNow Schedule Automation Using Change Request](#servicenow-schedule-automation-using-change-request)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
    - [Main Workflow](#main-workflow)
    - [Error Handling](#error-handling)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Features](#features)
  - [Future Enhancements](#future-enhancements)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
  - [Additional Information](#additional-information)

## Overview

This pre-built integrates with the [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow) to schedule an automation via Automation Catalog at the start of the **Planned start date**.


### Main Workflow

The main workflow in this pre-built is the `ServiceNow Create Change` workflow, which takes in several input variables required to create a change.

### Error Handling

If any task in the workflow fails to perform its expected function, the `taskError` job variable will hold the error message. If you are using any workflow in this pre-built as a childJob, you can use the `eval` task in Workflow Engine to check if the value of `taskError` is empty. If it is not empty, then the ServiceNow change that was created encountered an error.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2021.2`
* [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow)
  * `^2.2.0`

## Features

The main benefits and features of the pre-built are outlined below.

- Schedule an automation to run at the scheduled time in ServiceNow.


## Future Enhancements

The following is planned for a future release of this pre-built:

* Use the workflow name instead of the workflow id to schedule the automation.

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 
* The pre-built can be installed from within the pre-builts catalog. Simply search for the name of your desired pre-built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the pre-built:
* The planned start date/end date to run the automation can be modified in the service now account.
* Run the Operations Manager Automation `ServiceNow Schedule Automation Catalog Item At Start Date` or run workflow `ServiceNow Schedule Automation Catalog Item At Start Date` in a childJob task or by itself.
* An automation catalog item will be created in IAP to run the specified automation with the json form according to the schedule.

## Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
